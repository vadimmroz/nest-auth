import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { JwtService } from '@nestjs/jwt';
import { DatabaseService } from './database/database.service';
import { TestModule } from './test/test.module';
import { TestuserModule } from './testuser/testuser.module';

@Module({
  imports: [UserModule, AuthModule, TestModule, TestuserModule],
  controllers: [AppController],
  providers: [AppService, JwtService, DatabaseService],
})
export class AppModule {}
