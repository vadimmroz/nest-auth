import { Module } from '@nestjs/common';
import { TestController } from './test.controller';
import { TestService } from './test.service';
import {DatabaseService} from "../database/database.service";

@Module({
  controllers: [TestController],
  providers: [TestService, DatabaseService]
})
export class TestModule {}
