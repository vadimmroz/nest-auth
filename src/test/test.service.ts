import {Injectable} from '@nestjs/common';
import {DatabaseService} from "../database/database.service";

@Injectable()
export class TestService {
  constructor(private db: DatabaseService) {
  }

  getHello() {
    return this.db.test.findMany({
      where: {
        test: {
          endsWith: ''
        }
      }
    })
  }

  postHello(text: string) {
    return this.db.test.create({data: {test: text, counter: 0}})
  }

  update(text: string) {
    return this.db.test.update({
      where: {
        test: text
      },
      data: {
        counter: {
          increment: 1
        }
      }
    })
  }
}
