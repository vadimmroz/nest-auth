import {Controller, Get, Param} from '@nestjs/common';
import {TestService} from "./test.service";
import {Public} from "../public.decorator"

@Controller('test')
export class TestController {
  constructor(private testService: TestService) {
  }

  @Public()
  @Get()
  getHello() {
    return this.testService.getHello()
  }

  @Public()
  @Get("/:id")
  postHello(@Param('id') id: string) {
    return this.testService.postHello(id)
  }
  @Public()
  @Get("update/:id")
  updateHello(@Param('id') id: string) {
    return this.testService.update(id)
  }
}
