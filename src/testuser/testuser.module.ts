import { Module } from '@nestjs/common';
import { TestuserService } from './testuser.service';
import { TestuserController } from './testuser.controller';
import {DatabaseService} from "../database/database.service";

@Module({
  controllers: [TestuserController],
  providers: [TestuserService, DatabaseService],
})
export class TestuserModule {}
