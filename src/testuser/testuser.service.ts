import {Injectable} from '@nestjs/common';
import {DatabaseService} from "../database/database.service";

@Injectable()
export class TestuserService {
  constructor(private db: DatabaseService) {
  }


  // getUser() {
  //   return this.db.profile.findMany({select: {
  //     name: true,
  //       userId: true,
  //       user: true
  //     }})
  // }
  getUser() {
    return this.db.testUser.findMany({
      select: {
        email: true,
        id: true
      }
    })
  }

  createUser() {
    return this.db.testUser.create({
      data: {
        email: "test",
        profile: {}
      }
    })
  }

  createProfile() {
    return this.db.profile.create({
      data: {
        name: "test",
        age: "20",
        userId: 1,
        gender: "male",
      }
    })
  }
}