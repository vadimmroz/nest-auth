import {Controller, Get} from '@nestjs/common';
import {Public} from "../public.decorator"
import {TestuserService} from "./testuser.service";


@Controller('testuser')
export class TestuserController {
  constructor(private testUserService: TestuserService) {
  }
  @Public()
  @Get()
  getUser() {
    return this.testUserService.getUser()
  }
  @Get("createProfile")
  createProfile(){
    return this.testUserService.createProfile()
  }

}
