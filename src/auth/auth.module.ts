import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth.guard';
import {DatabaseService} from "../database/database.service";
import * as process from "process";

@Module({
  imports: [
    UserModule,
    JwtModule.register({
      global: true,
      secret: process.env.SECRET,
      signOptions: { expiresIn: '20m' },
    })
  ],
  providers: [AuthService,
    { provide: APP_GUARD, useClass: AuthGuard },
     DatabaseService],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
