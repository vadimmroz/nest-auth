import {ApiProperty} from '@nestjs/swagger';

export class User {
  @ApiProperty({example: "name", description: 'name'})
  name: string;

  @ApiProperty({
    example: '12345',
    description: 'password',
  })
  password: string
  @ApiProperty({
    example: 1234,
    description: 'date',
  })
  date: number
  @ApiProperty({
    example: 'admin',
    description: 'role',
  })
  role: string
  @ApiProperty({
    example: 'https://yt3.googleusercontent.com/-CFTJHU7fEWb7BYEb6Jh9gm1EpetvVGQqtof0Rbh-VQRIznYYKJxCaqv_9HeBcmJmIsp2vOO9JU=s900-c-k-c0x00ffffff-no-rj',
    description: 'avatar',
  })
  avatar: string
  @ApiProperty({
    example: "#16de7b",
    description: 'color',
  })
  color: string
}

export class UserLogin {
  @ApiProperty({example: "name", description: 'name'})
  name: string;
  @ApiProperty({example: "password", description: 'password'})
  password: string;

}