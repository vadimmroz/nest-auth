import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public } from '../public.decorator';
import { AuthGuard } from './auth.guard';
import {User, UserLogin} from "./entity/user.entity";

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
  @UseGuards(AuthGuard)
  @Get()
  findAll() {
    return this.authService.findAll();
  }
  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() signInDto: UserLogin) {
    return this.authService.signIn(signInDto.name, signInDto.password);
  }
  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('register')
  signUp(@Body() signUpDto: User) {
    return this.authService.signUp(signUpDto);
  }
}
