import {Injectable, UnauthorizedException} from '@nestjs/common';
import {UserService} from '../user/user.service';
import {JwtService} from '@nestjs/jwt';
import {User} from "./entity/user.entity";
import {DatabaseService} from "../database/database.service";

@Injectable()
export class AuthService {
  constructor(
    private readonly db: DatabaseService,
    private userService: UserService,
    private jwtService: JwtService
  ) {
  }

  async signIn(
    name: string,
    password: string,
  ): Promise<{ access_token: string }> {
    const user = await this.db.user.findFirst({where: {name: name}})
    if (user?.password !== password) {
      throw new UnauthorizedException();
    }
    const payload = {sub: user.id, name: user.name};
    return {access_token: await this.jwtService.signAsync(payload)};
  }

  async findAll() {
    return this.db.user.findMany({select: {name: true, avatar: true}});
  }

  async signUp(signUpDto: User) {
    await this.db.user.create({data: signUpDto})
    return this.signIn(signUpDto.name, signUpDto.password)
  }
}
