import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  public users = [
    {
      userId: 1,
      username: 'vadim',
      password: '12345',
    },
    {
      userId: 2,
      username: 'vika',
      password: '54321',
    },
  ];

  async findOne(username: string) {
    return this.users.find((user) => user.username === username);
  }
}
